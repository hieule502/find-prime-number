/* eslint-disable no-undef */
import { calculator } from './service';

test('prime service simple case', () => {
  const result = calculator.getNearestPrime('10');
  expect(result).toBe(7);
});

test('prime service complex case 1', () => {
  const result = calculator.getNearestPrime('123456');
  expect(result).toBe(123449);
});

test('prime service complex case 2', () => {
  const result = calculator.getNearestPrime('123456789');
  expect(result).toBe(123456761);
});
