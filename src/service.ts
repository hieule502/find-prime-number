const bigInt = require('big-integer')

class CalculationService {
  primes: number[];
  theLimitedOfSavedPrimes = 1000000;
  constructor () {
    this.primes = this.Sieve()
  }

  Sieve = () => {
    const primesResultArray = []
    // In general Sieve of Sundaram, produces
    // primes smaller than (2*x + 2) for a number
    // given number x. Since we want primes
    // smaller than n, we reduce n to half
    const theLimitedOfCheckIndex = (this.theLimitedOfSavedPrimes - 1) / 2

    // This array is used to separate
    // numbers of the form i+j+2ij from
    // others where 1 <= i <= j
    const marked = []

    // Initialize all elements as not marked
    for (let i = 0; i < theLimitedOfCheckIndex + 1; i++) {
      marked[i] = false
    }

    // Main logic of Sundaram.
    // Mark all numbers of the
    // form i + j + 2ij as true
    // where 1 <= i <= j
    for (let i = 1; i <= theLimitedOfCheckIndex; i++) {
      for (let j = i; (i + j + 2 * i * j) <= theLimitedOfCheckIndex; j++) {
        marked[i + j + 2 * i * j] = true
      }
    }

    // Since 2 is a prime number
    primesResultArray.push(2);

    // Print other primes.
    // Remaining primes are of
    // the form 2*i + 1 such
    // that marked[i] is false.
    for (let i = 1; i <= theLimitedOfCheckIndex; i++) {
      if (marked[i] === false) {
        primesResultArray.push(2 * i + 1)
      }
    }
    return primesResultArray;
  };

  getNearestPrime = (searchingNumberText: string) => {
    const searchingNumber = bigInt(searchingNumberText)
    const maxIntNumber = bigInt(Number.MAX_SAFE_INTEGER)
    if (searchingNumber < maxIntNumber) {
      const searchingNumberInInt = +searchingNumberText
      if (searchingNumberInInt === this.theLimitedOfSavedPrimes) {
        return this.primes[this.primes.length - 1]
      }
      if (searchingNumberInInt < this.theLimitedOfSavedPrimes) {
        return this.binarySearch(0, this.primes.length, searchingNumberInInt)
      } else {
        for (let n = searchingNumberInInt; n > this.theLimitedOfSavedPrimes; n--) {
          if (this.isPrime(n)) {
            return n
          }
        }
        return this.primes[this.primes.length - 1]
      }
    } else {
      const theLimitedOfSavedPrimesInBigInt = bigInt(this.theLimitedOfSavedPrimes)
      for (let n = searchingNumber; n > theLimitedOfSavedPrimesInBigInt;) {
        if (bigInt(n).isPrime()) {
          return n
        }
        n = n.minus(1)
      }
    }
    return 0
  };

  binarySearch (
    startIndex: number,
    endIndex: number,
    searchingNumber: number
  ): number {
    if (startIndex <= endIndex) {
      if (startIndex === endIndex) {
        // eslint-disable-next-line eqeqeq
        if (this.primes[startIndex] == searchingNumber) {
          return this.primes[startIndex]
        }
      }
      const mid = Math.floor((startIndex + endIndex) / 2)
      if (mid === 0 || mid === this.primes.length - 1) return this.primes[mid]
      // eslint-disable-next-line eqeqeq
      if (this.primes[mid] == searchingNumber) return this.primes[mid - 1]
      if (
        this.primes[mid] < searchingNumber &&
        this.primes[mid + 1] > searchingNumber
      ) { return this.primes[mid] }
      if (searchingNumber < this.primes[mid]) { return this.binarySearch(startIndex, mid - 1, searchingNumber) } else return this.binarySearch(mid + 1, endIndex, searchingNumber)
    }
    return 0
  }

  isPrime (checkNumber: number): boolean {
    if (checkNumber <= 1) return false
    for (let i = 2; i <= Math.sqrt(checkNumber); i++) {
      if (checkNumber % i === 0) {
        return false
      }
    }
    return true
  }
}

export const calculator = new CalculationService()
