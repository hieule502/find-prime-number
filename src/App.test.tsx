import React from 'react';
import { render, screen } from '@testing-library/react';
import App from './App';

test('renders Finding prime number title', () => {
  render(<App />);
  const linkElement = screen.getByText(/Finding prime number/i);
  expect(linkElement).toBeInTheDocument();
});
