// eslint-disable-next-line no-use-before-define
import React, { useState } from 'react'
import './App.css'
import { calculator } from './service'

function App () {
  const [inputValue, setInputValue] = useState('')
  const [outputValue, setOutputValue] = useState('')
  const [calculating, setCalculating] = useState(false)
  const [showingResult, setShowingResult] = useState(false)
  const [isWrongInput, setIsWrongInput] = useState(false)

  const onSubmitValue = (event: any) => {
    event.preventDefault()
    const isCorrectFormat = /^\d+$/.test(inputValue)
    if (isCorrectFormat) {
      setCalculating(true)
      const nearestValue = calculator.getNearestPrime(inputValue.toString())
      setOutputValue(nearestValue.toString())
      setCalculating(false)
      setShowingResult(true)
    } else {
      setIsWrongInput(true)
    }
  }

  const onchangeInputValue = (event: any) => {
    if (showingResult) {
      setShowingResult(false)
    }
    if (isWrongInput) {
      setIsWrongInput(false)
    }
    setInputValue(event.target.value)
  }

  return (
    <div className="App">
      <h1>Finding prime number</h1>
      <div className="content-wrapper">
        <div className="form-wrapper">
          <form onSubmit={onSubmitValue}>
            <label htmlFor="inputValue">Input: </label>
            <input
              value={inputValue}
              onChange={onchangeInputValue}
              type="text"
              id="inputValue"
              className="input-value"
            />
            <input className="submit-action" type="submit" value="Submit" />
          </form>
        </div>
        <div className="loader-container">
          {calculating && <div className="loader"></div>}
        </div>
        <div className="message-box">
          <div className="message-box-header">Notifycation:</div>
          {isWrongInput && <p>Please input number only!!</p>}
          {!isWrongInput && showingResult && (
            <p>
              The Nearest Prime Number with {inputValue} is: {outputValue}
            </p>
          )}

          {!isWrongInput && !showingResult && inputValue.length > 0 && (
            <p>Click Submit button to see the result</p>
          )}

          {!isWrongInput && !showingResult && inputValue.length === 0 && (
            <p>Please input value to find out awesome new!</p>
          )}
        </div>
      </div>
    </div>
  )
}

export default App
